package authorize

import (
	"dispatch/src/models/user"
	userService "dispatch/src/services/user"
	"dispatch/src/utils/response"
	"fmt"
	"github.com/gin-gonic/gin"
)


type Authorize struct {
}


func (Authorize) Auth(request *gin.Context)  {
	var response = response.Response{request}

	token := request.GetHeader("token")

	userService := new(userService.UserService)
	userInfo, err := userService.GetUserInfoByToken(token)
	if err != nil {
		fmt.Println("GetUserInfoByToken 失败")
		response.ErrorWithMSG("获取失败：非法的请求")
		request.Abort()
		return
	}

	if userInfo.Id == 0 {
		fmt.Println("check userInfo 失败")
		response.ErrorWithMSG("获取失败：用户信息异常")
		request.Abort()
		return
	}

	if !userInfo.OnJob() {
		fmt.Println("check user on job 失败")
		response.ErrorWithMSG("获取失败：用户信息异常")
		request.Abort()
		return
	}

	authModel := new(user.AuthModel)
	authModel.Login(&userInfo)

	request.Next()
}
