package routers

import (
	"dispatch/src/routers/user"
	"github.com/gin-gonic/gin"
)

// 初始化，注册路由
type RouterGroup struct {
	UserRouter user.UserRouter
}

func (group *RouterGroup) InitRouter(routerGroup *gin.RouterGroup) {
	group.UserRouter.InitUserRouter(routerGroup)
}